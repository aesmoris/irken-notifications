<?php

class notifications{

	public function set($type, $message, $autohide,$icon){

		//if(!isset($_SESSION["notifications"])){
		//}

		//$key = count($_SESSION["notifications"]);
		//$_SESSION["notifications"][$key]["type"] = $type;
		//$_SESSION["notifications"][$key]["message"] = $message;
		//$_SESSION["notifications"][$key]["autohide"] = $autohide;

		 if(!isset($_SESSION)) 
		    { 
		        session_start(); 
		    } 

		$_SESSION["notifications"]["type"] 		= $type;
		$_SESSION["notifications"]["message"] 	= $message;
		$_SESSION["notifications"]["autohide"]  =  $autohide;
		$_SESSION["notifications"]["icon"] 		=  $icon;
	}

	public function get(){

		if(!array_key_exists('notifications',$_SESSION)){
			return false;
		}else{
			$notifications =  $_SESSION["notifications"];
			unset($_SESSION["notifications"]);
			return $notifications;
		}

	}
}

?>